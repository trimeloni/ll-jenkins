#!/usr/bin/env bash

# Some standard first time ubuntu items
apt-get update
apt-get -y dist-upgrade
apt-get install -y git

# Install Jenkins - https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Ubuntu
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list
apt-get update
apt-get install -y jenkins

# Had an error - this seemed to work - http://askubuntu.com/questions/341955/problem-in-jenkins-installation-on-ubuntu-12-04-3-lts
apt-get clean
apt-get purge jenkins-common
apt-get install -y jenkins


# Install Node
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get update
apt-get install -y nodejs

# Pull down ngrok (so we can get an outside address to hit our box from)
apt-get install -y unzip
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
unzip ngrok-stable-linux-amd64.zip
mv ngrok /home/vagrant/ngrok

# Create ssh key and store in vagrant file
##ssh-keygen -t rsa -N "" -f /home/vagrant/.ssh/id_rsa
##chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
##chown vagrant:vagrant /home/vagrant/.ssh/id_rsa.pub
##chmod 600 /home/vagrant/.ssh/id_rsa
##chmod 644 /home/vagrant/.ssh/id_rsa.pub
# Need to get the host key into the key file without requiring a prompt
##su - vagrant -c "echo -e 'vagrant' | ssh -o StrictHostKeyChecking=no app@33.33.33.111 uptime"
#su - vagrant -c "echo -e 'vagrant' | ssh-copy-id vagrant@33.33.33.111"
##su - vagrant -c "echo -e 'vagrant' | ssh-copy-id app@33.33.33.111"

# Copy the already created key
mkdir -p /home/vagrant/.ssh
chown vagrant:vagrant /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh
cp /vagrant/id_rsa /home/vagrant/.ssh/id_rsa
cp /vagrant/id_rsa.pub /home/vagrant/.ssh/id_rsa.pub
chown vagrant:vagrant /home/vagrant/.ssh/id_rsa
chown vagrant:vagrant /home/vagrant/.ssh/id_rsa.pub
chmod 600 /home/vagrant/.ssh/id_rsa
chmod 644 /home/vagrant/.ssh/id_rsa.pub

# Run a vagrant command without strict host checking, this will add the machine into the recognized hosts
su - vagrant -c "echo -e 'vagrant' | ssh -o StrictHostKeyChecking=no app@33.33.33.111 uptime"

# All Set, give the user some information
echo 'Goto: http://33.33.33.110:8080'
echo 'To get Git integration, need to fire up ngrok or setup firewall forwarding and vm host forwarding...'
echo 'The value of /var/lib/jenkins/secrets/initialAdminPassword is: ' && cat /var/lib/jenkins/secrets/initialAdminPassword








