
Tutorial Link:
  https://wiki.jenkins-ci.org/display/JENKINS/Installing+Jenkins+on+Ubuntu - Install Jenkins
  https://code.tutsplus.com/tutorials/setting-up-continuous-integration-continuous-deployment-with-jenkins--cms-21511 - Jenkins Tutorial
  

L&L Links:
  Vagrant Setup: 
  Sample Simple Jenkins Node Program:


Before L&L
  do a vagrant up on the Deployment Box and then the Jenkins Box (doesn't really matter until we can auto-add the key... )

Setup a Box to put the Deployments
  vagrant up
    This will perform the following:
      install node
      Create an app user (user/pass: app/vagrant)
      Pull down the code from BitBucket
      Install "node forever" to run the app
      Startup the app
  Will show where to go to see the running sample application
  

Go into Jenkins Box and run Vagrant Up
  vagrant up
    This will perform the following:
      Install the Jenkins Server
  Setup Jenkins
    Goto: http://33.33.33.110:8080
    Install the suggested plugings plus the BitBucket plugin if you want to play with it via BitBucket instead of GitHub

  
Pull the Code locally onto Windows Box
  git clone git@bitbucket.org:trimeloni/hello-jenkins.git
  <make changes and stuff.. IN OTHER BRANCHES WOULD BE GREAT.. watch the awsomeness>



TODO:
  Store the public/provate keys in the git repo (won't matter because these currently only run and accessible to the local host)