#!/usr/bin/env bash

# Some standard first time ubuntu items
apt-get update
#apt-get -y dist-upgrade
apt-get install -y git

# Install Node
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get update
apt-get install -y nodejs

# Pull down ngrok (so we can get an outside address to hit our box from)
apt-get install -y unzip
wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
unzip ngrok-stable-linux-amd64.zip
rm ngrok-stable-linux-amd64.zip
mv ngrok /home/vagrant/ngrok

#
# Pull/Run the sample node program
#

# Add a user named "app" - maybe just use the vagrant user anyway in the future...
useradd app -s /bin/bash -m
usermod -a -G sudo app
chpasswd << 'END'
app:vagrant
END

# Pull the repo under the app user
cd /home/app
git clone https://trimeloni@bitbucket.org/trimeloni/hello-jenkins.git
cd hello-jenkins
npm install --production

# Install node plugin "forever" which allows an app to run non-stop
#   Use it to run our sample application
sudo npm install -g forever
forever start app.js

# Setup the pre-generated key
mkdir -p /home/app/.ssh
chown app:app /home/app/.ssh
chmod 700 /home/app/.ssh
cp /vagrant/authorized_keys /home/app/.ssh/authorized_keys
chown app:app /home/app/.ssh/authorized_keys
chmod 600 /home/app/.ssh/authorized_keys


# All Set, give the user some information
echo 'Pre-Shared key has been copied to the app users authorized_keys folder'
echo 'Node has been isntalled and the sample app has been downloaded and started'
echo 'Goto: http://33.33.33.111:5000'








